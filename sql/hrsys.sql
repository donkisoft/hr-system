-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 08:36 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrsys`
--

-- --------------------------------------------------------

--
-- Table structure for table `awarded_emp`
--

CREATE TABLE `awarded_emp` (
  `emp_id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `ctg_code` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctg_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `custom_id` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_name` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sr_code` char(6) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `deptid` int(11) NOT NULL,
  `dept_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dir_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`deptid`, `dept_name`, `dir_name`) VALUES
(10, 'Nhân sự', 'Hoàng Thu Hương'),
(20, 'Hành chính', 'Bùi Thanh Phương'),
(30, 'IT', 'Nguyễn Quang Thắng'),
(40, 'Phát triển', 'Mai Gia Thắng'),
(50, 'Phòng kiểm định', 'Vũ Thu Huyền');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `name`, `dept_id`, `age`, `sex`) VALUES
(3, 'Linh', 10, 26, '1'),
(4, 'Nam', 20, 26, '1');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade_point` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_code` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctg_code` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_start_date` char(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_price` int(11) DEFAULT NULL,
  `cost_price` int(11) DEFAULT NULL,
  `discount_limit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `emp_id` int(11) NOT NULL,
  `base_sal` int(11) DEFAULT NULL,
  `overtime_price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE `sales_details` (
  `slip_no` int(11) DEFAULT NULL,
  `product_code` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_tx`
--

CREATE TABLE `sales_tx` (
  `slip_no` int(11) DEFAULT NULL,
  `custom_code` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sr_code` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_date` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_date` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_amt` int(11) DEFAULT NULL,
  `discount_price` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `awarded_emp`
--
ALTER TABLE `awarded_emp`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`deptid`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `emp_dept_Key` (`dept_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `emp_dept_Key` FOREIGN KEY (`dept_id`) REFERENCES `department` (`deptid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
