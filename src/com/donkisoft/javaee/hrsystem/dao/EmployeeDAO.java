package com.donkisoft.javaee.hrsystem.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.donkisoft.javaee.hrsystem.model.Employee;

public class EmployeeDAO {
	private String jdbcURL;
	private String jdbcUsername;
	private String jdbcPassword;
	private Connection jdbcConnection;

	public EmployeeDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
		super();
		this.jdbcURL = jdbcURL;
		this.jdbcUsername = jdbcUsername;
		this.jdbcPassword = jdbcPassword;
	}

	protected void connect() throws SQLException {
		if (jdbcConnection == null || jdbcConnection.isClosed()) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				throw new SQLException(e);
			}
			jdbcConnection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		}
	}

	protected void disconnect() throws SQLException {
		if (jdbcConnection != null && !jdbcConnection.isClosed()) {
			jdbcConnection.close();
		}
	}

	public boolean insertEmployee(Employee employee) throws SQLException {
		String sql = "INSERT INTO Employee (name, dept_id, age, sex) VALUES (?, ?, ?, ?)";
		connect();

		PreparedStatement statement = jdbcConnection.prepareStatement(sql);
		statement.setString(1, employee.getName());
		statement.setInt(2, employee.getDeptId());
		statement.setInt(3, employee.getAge());
		statement.setString(4, employee.getSex());

		boolean rowInserted = statement.executeUpdate() > 0;
		statement.close();
		disconnect();
		return rowInserted;
	}

	public List<Employee> listAllEmployees() throws SQLException {
		List<Employee> listEmployee = new ArrayList<>();

		String sql = "SELECT * FROM Employee";

		connect();

		Statement statement = jdbcConnection.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);

		while (resultSet.next()) {
			int id = resultSet.getInt("emp_id");
			String name = resultSet.getString("name");
			int deptId = resultSet.getInt("dept_id");
			int age = resultSet.getInt("age");
			String sex = resultSet.getString("sex");

			Employee employee = new Employee(id, name, deptId, age, sex);
			listEmployee.add(employee);
		}

		resultSet.close();
		statement.close();

		disconnect();

		return listEmployee;
	}

	public boolean deleteEmployee(Employee employee) throws SQLException {
		String sql = "DELETE FROM Employee where emp_id = ?";

		connect();

		PreparedStatement statement = jdbcConnection.prepareStatement(sql);
		statement.setInt(1, employee.getId());

		boolean rowDeleted = statement.executeUpdate() > 0;
		statement.close();
		disconnect();
		return rowDeleted;
	}

	public boolean updateEmployee(Employee employee) throws SQLException {
		String sql = "UPDATE Employee SET name = ?, dept_id = ?, age = ?, sex = ?";
		sql += " WHERE emp_id = ?";
		connect();

		PreparedStatement statement = jdbcConnection.prepareStatement(sql);
		statement.setString(1, employee.getName());
		statement.setInt(2, employee.getDeptId());
		statement.setInt(3, employee.getAge());
		statement.setString(4, employee.getSex());

		boolean rowUpdated = statement.executeUpdate() > 0;
		statement.close();
		disconnect();
		return rowUpdated;
	}

	public Employee getEmployee(int id) throws SQLException {
		Employee employee = null;
		String sql = "SELECT * FROM Employee WHERE emp_id = ?";

		connect();

		PreparedStatement statement = jdbcConnection.prepareStatement(sql);
		statement.setInt(1, id);

		ResultSet resultSet = statement.executeQuery();

		if (resultSet.next()) {
			String name = resultSet.getString("name");
			int deptId = resultSet.getInt("dept_id");
			int age = resultSet.getInt("age");
			String sex = resultSet.getString("sex");

			employee = new Employee(id, name, deptId, age, sex);
		}

		resultSet.close();
		statement.close();

		return employee;
	}
}
