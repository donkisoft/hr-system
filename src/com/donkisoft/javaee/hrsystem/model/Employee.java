package com.donkisoft.javaee.hrsystem.model;

public class Employee {
	protected int id;
	protected String name;
	protected int deptId;
	protected int age;
	protected String sex;

	public Employee() {
		super();
	}

	public Employee(int id) {
		super();
		this.id = id;
	}

	public Employee(String name, int deptId, int age, String sex) {
		super();
		this.name = name;
		this.deptId = deptId;
		this.age = age;
		this.sex = sex;
	}

	public Employee(int id, String name, int deptId, int age, String sex) {
		super();
		this.id = id;
		this.name = name;
		this.deptId = deptId;
		this.age = age;
		this.sex = sex;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "Employee [id=" + this.id + ", name=" + this.name + ", deptId=" + this.deptId + ", age=" + this.age
				+ ", sex=" + this.sex + "]";
	}
}
